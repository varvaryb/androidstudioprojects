package com.example.aswitch;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        String user = getIntent().getExtras().getString("username");
        String gift = getIntent().getExtras().getString("gift");
        String sender = getIntent().getExtras().getString("sender");

        TextView infoTextView = findViewById(R.id.textViewInfo);
        if (!user.isEmpty() || !gift.isEmpty() || !sender.isEmpty()) {
            infoTextView.setText(user + ", Вам передали " + gift + " от " + sender);
        } else {
            infoTextView.setText("ЖЫвотное, вам передали дырку от бублика");
        }
    }
}