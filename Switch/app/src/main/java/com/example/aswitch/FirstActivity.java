package com.example.aswitch;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class FirstActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first);
    }

    public void onClick(View view) {
        EditText userEditText = findViewById(R.id.editTextUser);
        EditText giftEditText = findViewById(R.id.editTextGift);
        EditText senderEditText = findViewById(R.id.editTextSender);

        Intent intent = new Intent(FirstActivity.this, SecondActivity.class);
        intent.putExtra("username", userEditText.getText().toString());
        intent.putExtra("gift", giftEditText.getText().toString());
        intent.putExtra("sender", senderEditText.getText().toString());
        startActivity(intent);
    }
}