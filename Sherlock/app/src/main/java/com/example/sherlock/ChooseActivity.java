package com.example.sherlock;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class ChooseActivity extends AppCompatActivity {
    public final static String THIEF = "com.example.sherlock.THIEF";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose);
    }

    public void onRadioClick(View v) {
        Intent answerIntent = new Intent();

        switch (v.getId()) {
            case R.id.radio_dog:
                answerIntent.putExtra(THIEF, "Пёсик");
                break;
            case R.id.radio_crow:
                answerIntent.putExtra(THIEF, "Ворона");
                break;
            case R.id.radio_cat:
                answerIntent.putExtra(THIEF, "Кот Васька");
                break;

            default:
                break;
        }

        setResult(RESULT_OK, answerIntent);
        finish();
    }
}