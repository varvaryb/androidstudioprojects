package com.example.crowscounter;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private TextView mInfoTextView;
    private Button mCrowsCounterButton;
    private int mCrowCount = 0;
    private Button mCatsCounterButton;
    private int mCatCount = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mCrowsCounterButton = findViewById(R.id.buttonCrowsCounter);
        mCatsCounterButton = findViewById(R.id.buttonCatsCounter);
        mInfoTextView = findViewById(R.id.textView);

        mCrowsCounterButton.setOnClickListener(v ->
                mInfoTextView.setText("Я насчитал " + ++mCrowCount + " ворон"));

        mCatsCounterButton.setOnClickListener(v ->
                mInfoTextView.setText("Я насчитал " + ++mCatCount + " котов"));
    }

    public void onClick(View view) {
        mInfoTextView.setText("Hello Kitty!");
    }
}